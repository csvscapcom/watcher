package com.example.watcher;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity {

	//Initializes buttons and grid listing in the code
	Button createMovie, createTV;
	GridView watchList;
	
	//Values to store titles of movies/tv and episode number.
	private String creation_Title = "";
	private int creation_episodes = 0;
	
	//Test array to load into the GridView
	static final ArrayList<String> listing = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        listing.add("Alien");
        listing.add("Jobs");
        listing.add("Pulp Fiction");
        listing.add("Cabin in the Woods");
        listing.add("Law & Order: SVU: Season 3, Episode 1");
        listing.add("Spiderman 3");
        listing.add("Inglorious Bastards");
        listing.add("Hot Fuzz");
        listing.add("V for Vendetta");
        listing.add("Airplane");
        listing.add("Dodgeball");
        listing.add("Shawn of the Dead");
        listing.add("Horrible Bosses");


        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        //Loads in the grid view UI object and loads array into it
        watchList = (GridView) findViewById(R.id.toWatchList);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.gridviewtext, listing);
        watchList.setAdapter(adapter);
	    
	    //On touch, the name of the movie pops up (temporary until we add removal features to array)
		watchList.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(final AdapterView<?> parent, final View v,
				final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(parent.getItemAtPosition(position).toString());
                // Set up the buttons
                builder.setPositiveButton("Watched", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateRW(listing.get(position));
                        listing.remove(position);
                        adapter.notifyDataSetChanged();

                    }
                });
                builder.setNegativeButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listing.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });

                builder.show();
			}
		});
        adapter.notifyDataSetChanged();


	    //Links buttons to UI buttons
		createMovie = (Button)findViewById(R.id.addMovie);
		createTV = (Button)findViewById(R.id.addTV);

		//When Add Movie is clicked, pop dialogue box up to allow user to input a title.
		createMovie.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("What's the Title of the Movie?");

				// Set up the input
				final EditText input = new EditText(MainActivity.this);
				// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
				input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				builder.setView(input);

				// Set up the buttons
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				    	//Sets title to the user input
				        creation_Title = input.getText().toString();
                        listing.add(creation_Title);
                        adapter.notifyDataSetChanged();
				    }
				});
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				        dialog.cancel();
				    }
				});

				builder.show();
			}
		});
		
		//When Add TV is clicked, pop dialogue box up to allow user to input a title.
		createTV.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("What's the Title of the TV Show?");

				// Set up the input
				final EditText input = new EditText(MainActivity.this);
				// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
				input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
				builder.setView(input);

				// Set up the buttons
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				    	//Sets the title to the TV Show title that the user inputted
				        creation_Title = input.getText().toString();
                        listing.add(creation_Title);
                        adapter.notifyDataSetChanged();
				    }
				});
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				        dialog.cancel();
				    }
				});

				builder.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_movie:
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("What's the Title of the Movie?");

                // Set up the input
                final EditText input = new EditText(MainActivity.this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Sets title to the user input
                        creation_Title = input.getText().toString();
                        listing.add(creation_Title);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            case R.id.add_tv:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                builder2.setTitle("What's the Title of the TV Show?");

                // Set up the input
                final EditText input2 = new EditText(MainActivity.this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                builder2.setView(input2);

                // Set up the buttons
                builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Sets the title to the TV Show title that the user inputted
                        creation_Title = input2.getText().toString();
                        listing.add(creation_Title);
                    }
                });
                builder2.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder2.show();
                break;
            case R.id.action_settings:

        }
        return true;
    }

    public void updateRW(String input){
        TextView view1 = (TextView) findViewById(R.id.recentlyWatched1);
        TextView view2 = (TextView) findViewById(R.id.recentlyWatched2);
        TextView view3 = (TextView) findViewById(R.id.recentlyWatched3);

        view3.setText(view2.getText());
        view2.setText(view1.getText());
        view1.setText(input);

    }
}